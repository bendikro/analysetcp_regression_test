#! /usr/bin/env python

from __future__ import print_function

import re, os, sys
import argparse

try:
    from termcolor import colored, cprint
    termcolor = True
except:
    print("termcolor could not be found. To enable colors in terminal output, install termcolor.")
    termcolor = False
    def cprint(*arg, **kwargs):
        print_(*arg)
    def colored(text, color):
        return text

command = "analyseTCP -s %(src_ip)s %(dst_ip)s -f %(sender_file)s %(dst_port)s %(src_port)s %(receiver_file)s %(options)s > %(output_file)s 2> %(stderr_file)s"
default_opts = { "src_ip": "", "dst_ip": "", "src_port": "", "dst_port": "", "src_port": "", "receiver_file": "", "options": "" }

tests = {}

def add_test(file_dump, name, test_name, command_opts, keys, stderr_keys=None):
    if not file_dump in tests:
        tests[file_dump] = {"name": name, "tests": {}}
    tests[file_dump]["tests"][test_name] = {"command_opts": command_opts, "keys": keys, "stderr_keys": stderr_keys}

fdump = "received_old_seq_after_seq_wrap/16_thin_rdb_vs_16_thick_stream_cap_2000kbit_duration_30m_payload_100_itt_100_rtt_100_loss__p_in_flight_3_num_2_zsender.pcap"
name = "received_old_seq_after_seq_wrap"
add_test(fdump, name, "test2", command_opts={"src_ip" : "10.0.0.13", "src_port" : "-q 12000"},
         keys=[
             ("Total packets sent (adj. for fragmentation)", "(?P<value>\d+).*", 19817),
             ("Total packets sent (found in dump)", "(?P<value>\d+).*", 17161),
             ("Total data packets sent (adj.)", "(?P<value>\d+).*", 19814),
             ("Total data packets sent (found)", "(?P<value>\d+).*", 17158),
             ("Total pure acks (no payload)", "(?P<value>\d+).*", 2),
             ("SYN/FIN/RST packets sent", "(?P<value>\d+/\d+/\d+).*", "1/1/0"),
             ("Number of retransmissions", "(?P<value>\d+).*", 4859),
             ("Number of packets with bundled segments", "(?P<value>\d+).*", 0),
             ("Number of received acks", "(?P<value>\d+).*", 15822),
             ("Total bytes sent (payload)", "(?P<value>\d+).*", 30647028),
             ("Number of unique bytes", "(?P<value>\d+).*", 23611196),
             ("Number of retransmitted bytes", "(?P<value>\d+).*", 7035832),
             ("Redundant bytes (bytes already sent)", "(?P<value>.+).*", "7035832 (22.96 %)"),
             ("Based on sent pkts (adj. for fragmentation)", "(?P<value>.+).*", "24.52 %"),
             ("Based on sent pkts (found in dump)", "(?P<value>.+).*", "28.31 %"),
         ])


fdump = "512_vs_512_streams_segmentation_offloading/fair-stable-1000kbit-rtt150-cubic-pfifo-900s-512vs512-streams-itt100-ps120-_front.dump"
name = "more_tests2"
add_test(fdump, name, "test3", command_opts={ "src_ip" : "10.0.0.10", "src_port" : "-q 15013" },
         keys=[
             ("Total packets sent (adj. for fragmentation)", "(?P<value>\d+).*", 109),
             ("Total packets sent (found in dump)", "(?P<value>\d+).*", 108),
             ("Total data packets sent (adj.)", "(?P<value>\d+).*", 105),
             ("Total data packets sent (found)", "(?P<value>\d+).*", 104),
             ("Total pure acks (no payload)", "(?P<value>\d+).*", 2),
             ("SYN/FIN/RST packets sent", "(?P<value>\d+/\d+/\d+).*", "1/0/1"),
             ("Number of retransmissions", "(?P<value>\d+).*", 67),
             ("Number of packets with bundled segments", "(?P<value>\d+).*", 0),
             ("Number of received acks", "(?P<value>\d+).*", 37),
             ("Total bytes sent (payload)", "(?P<value>\d+).*", 147732),
             ("Number of unique bytes", "(?P<value>\d+).*", 52140),
             ("Number of retransmitted bytes", "(?P<value>\d+).*", 95592),
             ("Redundant bytes (bytes already sent)", "(?P<value>.+).*", "95592 (64.71 %)"),
             ("Based on sent pkts (adj. for fragmentation)", "(?P<value>.+).*", "61.47 %"),
             ("Based on sent pkts (found in dump)", "(?P<value>.+).*", "62.04 %"),
         ])

add_test(fdump, name, "test_multiple_syns", command_opts={"src_ip" : "10.0.0.10", "src_port" : "-q 15387"},
         keys=[
             ("Total packets sent (adj. for fragmentation)", "(?P<value>\d+).*", 277),
             ("Total packets sent (found in dump)", "(?P<value>\d+).*", 276),
             ("Total data packets sent (adj.)", "(?P<value>\d+).*", 270),
             ("Total data packets sent (found)", "(?P<value>\d+).*", 269),
             ("Total pure acks (no payload)", "(?P<value>\d+).*", 2),
             ("SYN/FIN/RST packets sent", "(?P<value>\d+/\d+/\d+).*", "4/3/1"),
             ("Number of retransmissions", "(?P<value>\d+).*", 152),
             ("Number of packets with bundled segments", "(?P<value>\d+).*", 0),
             ("Number of received acks", "(?P<value>\d+).*", 123),
             ("Total bytes sent (payload)", "(?P<value>\d+).*", 388996),
             ("Number of unique bytes", "(?P<value>\d+).*", 173740),
             ("Number of retransmitted bytes", "(?P<value>\d+).*", 215256),
             ("Redundant bytes (bytes already sent)", "(?P<value>.+).*", "215256 (55.34 %)"),
             ("Based on sent pkts (adj. for fragmentation)", "(?P<value>.+).*", "54.87 %"),
             ("Based on sent pkts (found in dump)", "(?P<value>.+).*", "55.07 %"),
         ])

add_test(fdump, name, "test_rst_at_beginning", command_opts={"src_ip" : "10.0.0.10", "src_port" : "-q 15090" },
         keys=[
             ("Total packets sent (adj. for fragmentation)", "(?P<value>\d+).*", 205),
             ("Total packets sent (found in dump)", "(?P<value>\d+).*", 201),
             ("Total data packets sent (adj.)", "(?P<value>\d+).*", 200),
             ("Total data packets sent (found)", "(?P<value>\d+).*", 196),
             ("Total pure acks (no payload)", "(?P<value>\d+).*", 2),
             ("SYN/FIN/RST packets sent", "(?P<value>\d+/\d+/\d+).*", "2/1/1"),
             ("Number of retransmissions", "(?P<value>\d+).*", 113), # tcptrace gives 112 because of the RST packet in the beginning
             ("Number of packets with bundled segments", "(?P<value>\d+).*", 0),
             ("Number of received acks", "(?P<value>\d+).*", 88),
             ("Total bytes sent (payload)", "(?P<value>\d+).*", 286288),
             ("Number of unique bytes", "(?P<value>\d+).*", 124100),
             ("Number of retransmitted bytes", "(?P<value>\d+).*", 162188),
             ("Redundant bytes (bytes already sent)", "(?P<value>.+).*", "162188 (56.65 %)"),
             ("Based on sent pkts (adj. for fragmentation)", "(?P<value>.+).*", "55.12 %"),
             ("Based on sent pkts (found in dump)", "(?P<value>.+).*", "56.22 %"),
         ])



fdump = "segments_spanning_multiple_ranges/fair-stable-5000kbit-rtt150-cubic-pfifo-1200s-30vs30-streams-itt100-ps120-_front.dump"
name = "more_tests3"
add_test(fdump, name, "test_spanning_multple_segments", command_opts={"src_ip" : "10.0.0.11", "options" : "-A" },
         keys=[
             ("Total packets sent (adj. for fragmentation)", "(?P<value>\d+).*", 16266),
             ("Total packets sent (found in dump)", "(?P<value>\d+).*", 15374),
             ("Total pure acks (no payload)", "(?P<value>\d+).*", 2),
             ("SYN/FIN/RST packets sent", "(?P<value>\d+/\d+/\d+).*", "1/1/0"),
             ("Number of retransmissions", "(?P<value>\d+).*", 1623),
             ("Number of packets with bundled segments", "(?P<value>\d+).*", 52),
             ("Number of received acks", "(?P<value>\d+).*", 13307),
             ("Total bytes sent (payload)", "(?P<value>\d+).*", 24399692),
             ("Number of unique bytes", "(?P<value>\d+).*", 21952560),
             ("Number of retransmitted bytes", "(?P<value>\d+).*", 2373284),
             ("Redundant bytes (bytes already sent)", "(?P<value>.+).*", "2447132 (10.03 %)"),
             ("Based on sent pkts (adj. for fragmentation)", "(?P<value>.+).*", "9.98 %"),
             ("Based on sent pkts (found in dump)", "(?P<value>.+).*", "10.56 %"),
         ])

fdump = "missing_syn_ack_ack/fair-stable-1-1200secs-5000kbit-rtt150-tcp_cubic-opts1-short-30vs30-itt100-ps120-_front.dump"
name = "more_tests4"
add_test(fdump, name, "test_missing_pure_ack_on_the_syn_ack", command_opts={"src_ip" : "10.0.0.10", "dst_ip" : "-r 10.0.1.10", "dst_port" : "-p 23476", "options" : "-A" },
         keys=[
             ("Total packets sent", "(?P<value>\d+).*", 8016),
             ("Total data packets sent", "(?P<value>\d+).*", 8012),
             ("Total pure acks (no payload)", "(?P<value>\d+).*", 3),
             ("SYN/FIN/RST packets sent", "(?P<value>\d+/\d+/\d+).*", "1/2/0"),
             ("Number of retransmissions", "(?P<value>\d+).*", 1703),
             ("Number of packets with bundled segments", "(?P<value>\d+).*", 0),
             ("Number of received acks", "(?P<value>\d+).*", 6317),
             ("Total bytes sent (payload)", "(?P<value>\d+).*", 1800368),
             ("Number of unique bytes", "(?P<value>\d+).*", 1402800),
             ("Number of retransmitted bytes", "(?P<value>\d+).*", 397568),
             ("Redundant bytes (bytes already sent)", "(?P<value>.+).*", "397568 (22.08 %)"),
             ("Estimated loss rate based on retransmissions", "(?P<value>.+).*", "21.25 %"),
         ])

add_test(fdump, name, "test_dupacks", command_opts={"src_ip" : "10.0.0.10", "src_port" : "-q 20029", "options" : "-v4" },
         keys=[
             ("Max retransmissions", "(?P<value>.+).*", "6"),
             ("1. retransmission (count / accumulated)", "(?P<value>.+).*", "1308 / 1720"),
             ("2. retransmission (count / accumulated)", "(?P<value>.+).*", "344 / 412"),
             ("3. retransmission (count / accumulated)", "(?P<value>.+).*", "57 / 68"),
             ("4. retransmission (count / accumulated)", "(?P<value>.+).*", "7 / 11"),
             ("5. retransmission (count / accumulated)", "(?P<value>.+).*", "3 / 4"),
             ("6. retransmission (count / accumulated)", "(?P<value>.+).*", "1 / 1"),
             ("Max dupacks",                             "(?P<value>.+).*", "10"),
             ("1. dupacks (count / accumulated)", "(?P<value>.+).*", "875 / 2323"),
             ("2. dupacks (count / accumulated)", "(?P<value>.+).*", "618 / 1448"),
             ("3. dupacks (count / accumulated)", "(?P<value>.+).*", "395 / 830"),
             ("4. dupacks (count / accumulated)", "(?P<value>.+).*", "235 / 435"),
             ("5. dupacks (count / accumulated)", "(?P<value>.+).*", "95 / 200"),
             ("6. dupacks (count / accumulated)", "(?P<value>.+).*", "45 / 105"),
             ("7. dupacks (count / accumulated)", "(?P<value>.+).*", "28 / 60"),
             ("8. dupacks (count / accumulated)", "(?P<value>.+).*", "20 / 32"),
             ("9. dupacks (count / accumulated)", "(?P<value>.+).*", "10 / 12"),
             ("10. dupacks (count / accumulated)", "(?P<value>.+).*", "2 / 2"),
         ])


fdump = "seq_wrap_to_0/t1-tcp_vs_g1_kbit10000_min20_ps120_itt100_rtt150_loss_pif0_qlen122_delayfixed_num0_clps-t-0_clps-g-0_soff-off_soff-t-0_soff-g-0_brate-speed-10-duplex-full_zsender.pcap"
name = "more_tests5"
add_test(fdump, name, "test_seq_wrap_to_0", command_opts={"src_ip" : "10.0.0.13", "dst_ip" : "-r 10.0.0.22", "src_port" : "-q 12031", "options" : ""
                                                          "-g test_dumps/seq_wrap_to_0/t1-tcp_vs_g1_kbit10000_min20_ps120_itt100_rtt150_loss_pif0_qlen122_delayfixed_num0_clps-t-0_clps-g-0_soff-off_soff-t-0_soff-g-0_brate-speed-10-duplex-full_zreceiver.pcap"},
         keys=[
             ("Total packets sent", "(?P<value>\d+).*", 90486),
             ("Total data packets sent", "(?P<value>\d+).*", 90483),
             ("Total pure acks (no payload)", "(?P<value>\d+).*", 2),
             ("SYN/FIN/RST packets sent", "(?P<value>\d+/\d+/\d+).*", "1/1/0"),
             ("Number of retransmissions", "(?P<value>\d+).*", 1528),
             ("Number of packets with bundled segments", "(?P<value>\d+).*", 0),
             ("Number of received acks", "(?P<value>\d+).*", 56086),
             ("Total bytes sent (payload)", "(?P<value>\d+).*", 130997684),
             ("Number of unique bytes", "(?P<value>\d+).*", 128785140),
             ("Number of retransmitted bytes", "(?P<value>\d+).*", 2212544),
             ("Redundant bytes (bytes already sent)", "(?P<value>.+).*", "2212544 (1.69 %)"),
             ("Estimated loss rate based on retransmissions", "(?P<value>.+).*", "1.69 %"),
         ])

fdump = "range_loss_fail/20140604_210217-0-dur_60s-bw_5000kbit-rtt_150ms-queue_short_pfifo-fack1-earlretr2-retrclps1-lolat0-dsack1-winscale1-sack1-10greedy-port_15000-greedy_10-cc_reno-rampup_100:0ms-rate_5000kbit_front.dump"
name = "range_loss_fail"
regex = "\s*(?P<duration>\d+)\s+(?P<loss_est>\S+)(:?\s\/\s(?P<loss_est_adj>\S+))?\s+(?P<packets_sent>\S+)\s+(?P<packets_recv>\S+)\s+(?P<packet_loss>\S+)\s%\s+(?P<byte_loss>\S+)\s%\s+(?P<range_loss>\S+)\s%"
add_test(fdump, name, "actual_range_loss_fail",
         command_opts={"src_ip" : "10.0.0.11", "dst_ip" : "-r 10.0.1.10", "src_port" : "", "options" : "-e", "receiver_file":
                       "-g test_dumps/range_loss_fail/20140604_210217-0-dur_60s-bw_5000kbit-rtt_150ms-queue_short_pfifo-fack1-earlretr2-retrclps1-lolat0-dsack1-winscale1-sack1_after.dump"},
         keys=[
             ("10.0.0.11:15000-10.0.1.10:12345", regex,
              {"duration": "64", "loss_est": "4.08", "loss_est_adj": "4.08", "packets_sent": "1029", "packets_recv": "988", "packet_loss": "3.98", "byte_loss": "3.57", "range_loss": "3.99"}),

             ("10.0.0.11:15001-10.0.1.10:12345", regex,
              {"duration": "64", "loss_est": "2.54", "loss_est_adj": "2.54", "packets_sent": "2444", "packets_recv": "2383", "packet_loss": "2.50", "byte_loss": "2.28", "range_loss": "2.50"}),

             ("10.0.0.11:15002-10.0.1.10:12345", regex,
              {"duration": "65", "loss_est": "1.94", "loss_est_adj": "1.94", "packets_sent": "2625", "packets_recv": "2575", "packet_loss": "1.90", "byte_loss": "1.80", "range_loss": "1.91"}),

             ("10.0.0.11:15003-10.0.1.10:12345", regex,
              {"duration": "64", "loss_est": "2.70", "loss_est_adj": "2.70", "packets_sent": "2629", "packets_recv": "2559", "packet_loss": "2.66", "byte_loss": "2.35", "range_loss": "2.66"}),

             ("10.0.0.11:15004-10.0.1.10:12345", regex,
              {"duration": "64", "loss_est": "2.35", "loss_est_adj": "2.35", "packets_sent": "2128", "packets_recv": "2079", "packet_loss": "2.30", "byte_loss": "2.32", "range_loss": "2.30"}),

             ("10.0.0.11:15005-10.0.1.10:12345", regex,
              {"duration": "65", "loss_est": "2.12", "loss_est_adj": "2.12", "packets_sent": "3201", "packets_recv": "3133", "packet_loss": "2.12", "byte_loss": "1.82", "range_loss": "2.13"}),

             ("10.0.0.11:15006-10.0.1.10:12345", regex,
              {"duration": "65", "loss_est": "3.57", "loss_est_adj": "3.57", "packets_sent": "2490", "packets_recv": "2402", "packet_loss": "3.53", "byte_loss": "3.11", "range_loss": "3.54"}),

             ("10.0.0.11:15007-10.0.1.10:12345", regex,
              {"duration": "66", "loss_est": "3.35", "loss_est_adj": "3.35", "packets_sent": "2657", "packets_recv": "2568", "packet_loss": "3.35", "byte_loss": "3.01", "range_loss": "3.35"}),

             ("10.0.0.11:15008-10.0.1.10:12345", regex,
              {"duration": "64", "loss_est": "2.21", "loss_est_adj": "2.21", "packets_sent": "2901", "packets_recv": "2837", "packet_loss": "2.21", "byte_loss": "1.93", "range_loss": "2.21"}),

             ("10.0.0.11:15009-10.0.1.10:12345", regex,
              {"duration": "64", "loss_est": "1.92", "loss_est_adj": "1.92", "packets_sent": "2349", "packets_recv": "2305", "packet_loss": "1.87", "byte_loss": "1.81", "range_loss": "1.87"}),
         ])


# Some IP packets in this trace reports ipSize=0. This can cause incorrect size calculationi if not handled.
fdump = "incorrect_ip_size/mqtt-64_reference_nu.pcapng"
name = "incorrect_ip_size"
add_test(fdump, name, "incorrect_ip_size_test1", command_opts={"src_ip" : "192.174.136.246", "src_port" : "-q 1883", "options" : "-v0" },
         keys=[],
         stderr_keys=[
             ("192.174.136.246-1883-192.174.127.245-49183: (seq: 2209645540, 2209646419): Length reported as 0, but correct value is 919."),
             ("192.174.136.246-1883-192.174.127.245-49183: (seq: 2209647298, 2209648370): Length reported as 0, but correct value is 1112."),
             ("192.174.136.246-1883-192.174.127.245-49184: (seq: 1603422783, 1603423855): Length reported as 0, but correct value is 1112."),
             ("192.174.136.246-1883-192.174.127.245-49184: (seq: 1603438067, 1603439139): Length reported as 0, but correct value is 1112."),
         ])


def read_output_info(filename):
    d = {}
    f = open(filename, "r")
    for line in f.readlines():
        split = line.split(": ")
        if len(split) == 2:
            d[split[0].strip()] = split[1].strip()
    return d

def test_stderr_output(name, test_name, keys, filename):
    print("filename")
    f = open(filename, "r")
    ret = []
    d = {}
    for l in f.readlines():
        l = l.strip()
        d[l] = None

    for k in keys:
        if not k in d:
            cprint("Expected stderr value not found for test '%s' (Filename: %s)" % (test_name, filename), "red")
            print("Expected stderr line: '%s'" % k)
            ret.append(k)
    return ret

class Properties(dict):
    def __init__(self, *args, **kwargs):
        super(Properties, self).__init__(*args, **kwargs)
        self.__dict__ = self

def test_output_data(name, test_name, keys, filename):
    data = []
    info = read_output_info(filename)
    #print("filename: %s" % filename)
    p = Properties()
    p.file_test_error_printed = False
    file_error_printed = False

    def print_file_test_error():
        if p.file_test_error_printed is False:
            cprint("Error when processing test '%s' (Filename: %s)!" % (test_name, filename), "red")
        p.file_test_error_printed = True

    for key, regex, expected in keys:
        if not key in info:
            print_file_test_error()
            cprint("The key '%s' was not found in the results output!" % key, "red")
            continue
        m = re.match(regex, info[key], flags=re.DOTALL)
        if not m:
            if not file_error_printed:
                file_error_printed = True
                fail_str = "Failed to match '%s' with regex '%s'" % (info[key], regex)
                data.append(fail_str)
                print(fail_str)
            continue

        fail_str = None
        if type(expected) is dict:
            if m.groupdict() != expected:
                fail_str = "For key: '%s', \nFound value: '%s', \nexpected:    '%s'" % (key, m.groupdict(), expected)
        elif m.group("value") != str(expected):
            fail_str = "For key: '%s', Found value: '%s', expected '%s'" % (key, m.group("value"), expected)

        if fail_str:
            print_file_test_error()
            data.append(fail_str)
            print(fail_str)
    return data

def run_test(name, test_name, absolute_filepath, filepath, filename, args):
    opts = default_opts.copy()
    opts.update(tests[filepath]["tests"][test_name]["command_opts"])
    opts["sender_file"] = absolute_filepath
    opts["output_file"] = "%s.output" % (os.path.join(args.tmp_directory, "%s_%s" % (test_name, filename)))
    opts["stderr_file"] = "%s.stderr" % (os.path.join(args.tmp_directory, "%s_%s" % (test_name, filename)))

    print("\nRunning test: %s:%s" % (name, test_name))
    cmd = command % opts
    cprint("COMMAND: %s" % cmd, "yellow")
    os.system(cmd)
    return opts

def do_test(tests_to_run, args):
    tests_with_errors = []
    errors = []
    for test in tests_to_run:
        name, test_name, filepath, f, basename, args = test
        opts = run_test(name, test_name, filepath, f, basename, args)
        data = test_output_data(name, test_name, tests[f]["tests"][test_name]["keys"], opts["output_file"])
        if opts.get("stderr_keys", None) is not None:
            ret = test_stderr_output(name, test_name, tests[f]["tests"][test_name]["stderr_keys"], opts["stderr_file"])
        # Fails
        if data:
            tests_with_errors.append((test, errors))
    return tests_with_errors

def get_files(d):
    files = []
    for f in tests.keys():
        files.append((f, os.path.join(d, f)))
    return files

def list_tests():
    print("\nTests:")
    for f in tests.keys():
        name = tests[f]["name"]
        print(" - %s (File dump: %s)" % (name, f))
        for k in tests[f]["tests"]:
            print("    * %s:%s" % (name, k))
    print()

def get_tests_to_run(test_id, args):
    tests_to_run = []
    #print("get_tests_to_run:", test_id)

    for f in tests:
        filepath = os.path.join(args.directory, f)
        #print("filepath: %s : isfile: %d" % (filepath, os.path.isfile(filepath)))
        if not os.path.isfile(filepath):
            continue
        # All all the tests for this file
        if test_id == None or f == test_id or tests[f]["name"] == test_id:
            basename = os.path.basename(f)
            for test_name in tests[f]["tests"]:
                tests_to_run.append((tests[f]["name"], test_name, filepath, f, basename, args))
        else:
            for test_name in tests[f]["tests"]:
                name = tests[f]["name"]
                test_name_group = "%s:%s" % (name, test_name)
                if test_name_group == test_id:
                    basename = os.path.basename(f)
                    tests_to_run.append((tests[f]["name"], test_name, filepath, f, basename, args))
    return tests_to_run

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description="Run regression test for analyseTCP")
    argparser.add_argument("-tmp", "--tmp-directory", default="tmp_dir", help="The tmp directory to store files.", required=False)
    argparser.add_argument("-f", "--file", default="", help="A tcpdump file to process.", required=False)
    argparser.add_argument("-d", "--directory", default="test_dumps", help="A directory contining tcpdump files.", required=False)
    argparser.add_argument("-t", "--test", default="", help="The test to run.", required=False)
    argparser.add_argument("-l", "--list-tests", action='store_true', required=False, default=False)

    args = argparser.parse_args()

    def help_and_exit(msg):
        print("\n" + colored(msg, "red") + "\n")
        argparser.print_help()
        sys.exit(0)

    if args.list_tests:
        list_tests()
        sys.exit(0)

    if args.file:
        tests_to_run = get_tests_to_run(args.file, args)
    elif args.test:
        tests_to_run = get_tests_to_run(args.test, args)
    else:
        files = get_files(args.directory)
        tests_to_run = []
        for f, filepath in files:
            tests_to_run += get_tests_to_run(f, args)

    if not os.path.isdir(args.tmp_directory):
        os.makedirs(args.tmp_directory)

    errors = do_test(tests_to_run, args)
    if errors:
        print("Tests with errors:")
        for test, errors in errors:
            name, test_name, filepath, f, basename, args = test
            print("  * %s:%s" % (name, test_name))
